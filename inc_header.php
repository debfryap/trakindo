<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>WEBARQ - Static Website</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="icon" href="favicon.ico">
<!--Style-->
<link rel="stylesheet" href="css/reset.css">
<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/style.css">
<!--js-->
<script src="js/vendor/jquery-1.9.1.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
<script src="js/plugins.js"></script>
<script src="js/vendor/modernizr-2.6.2.min.js"></script>
<script type="text/javascript" src="js/jquery.nivo.slider.js"></script>
<script src="js/jquery_function.js"></script>
</head>
<body>
<!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

<!-- Add your site or application content here -->

<!-- header -->
<header>
  <div class="head-wrap">
    <div class="logo left">
      <a href="#"><img src="images/material/logo.jpg" alt=""></a>
    </div>
    <div class="right">
      <div class="top-menu">
        <ul>
          <li>
            <a href="#">What’s New</a>
          </li>
          <li>
            <a href="#">News & Events</a>
          </li>
          <li>
            <a href="#">Human Capital</a>
          </li>
          <li>
            <a href="#">Privacy Policy</a>
          </li>
          <li>
            <a href="#">Sitemap</a>
          </li>
          <li>
            <a href="#">Contact  Us</a>
          </li>
        </ul>
        <form action="" method="get">
          <input name="" type="text" placeholder="Search">
          <input name="" type="submit" value=" ">
        </form>
      </div>
      <div class="bottom-menu">
        <ul>
          <li>
            <a href="#"><img src="images/material/icon-home.png" alt=""></a>
          </li>
          <li>
            <a href="#">The Company</a>
          </li>
          <li>
            <a href="#">INDUSTRy</a>
          </li>
          <li>
            <a href="#">Product</a>
          </li>
          <li>
            <a href="#">product support</a>
          </li>
          <li>
            <a href="#">CORPORATE CITIZENSHIP</a>
          </li>
          <li>
            <a href="#" class="red">PARTSTORE ONLINE</a>
          </li>
        </ul>
      </div>
    </div>
    <div class="clear"></div>
  </div>
</header>
<!-- end of header -->