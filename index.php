<?php include('inc_header.php');?>
<!-- middle -->
<section id="main-banner">
  <div class="slider-wrapper theme-default">
    <div id="slider" class="nivoSlider">
      <img src="images/slider/banner.jpg" alt="" title="#htmlcaption"/>
      <img src="images/slider/banner-02.jpg" alt="" title="#htmlcaption2" />
      <img src="images/slider/banner-03.jpg" alt="" title="#htmlcaption"/>
      <img src="images/slider/banner-04.jpg" alt="" title="#htmlcaption2" />
    </div>
    <div id="htmlcaption" class="nivo-html-caption">
      <div class="wrap-cap">
        <h3>Our Commitment Customer Success</h3>
        <p>The Affiliates offers a comprehensive range of services to customers in several inter-related business lines as One Stop Shop Solution</p>
        <a href="#" class="more">Read More</a>
      </div>
    </div>
    <div id="htmlcaption2" class="nivo-html-caption">
      <div class="wrap-cap">
        <h3>Our Commitment Customer Success2</h3>
        <p>The Affiliates offers a comprehensive range of services to customers in several inter-related business lines as One Stop Shop Solution</p>
        <a href="#" class="more">Read More</a>
      </div>
    </div>
  </div>
</section>
<section> <div class="wrap-content"><h4>Company Overview</h4> <h3>PT Trakindo Utama (Trakindo) is the authorized dealer
in Indonesia for Caterpillar products, </h3><p>Trakindo is the authorized dealer in Indonesia for Caterpillar products, the world's largest manufacturer of heavy equipment, diesel and natural gas engines, industrial engines and generator sets. Trakindo was established in 1970 by the founder, Mr. AHK Hamami. The Company became the authorized dealer for Caterpillar in 1971 and now has more than 70 branches throughout the country from Sumatera to Papua. </p> 
<a href="#" class="readmore">Read More</a>
</div> </section>
<!-- end of middle -->
<script>
$(window).load(function() {
        $('#slider').nivoSlider({
			directionNav: false,
			controlNav: false,
			effect: 'fold',
		});
    });
</script>
<?php include('inc_footer.php');?>