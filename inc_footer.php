<!--Footer -->

<footer>
  <div class="wrap-content">
    <div><p class="phone"><span>Trakindo</span> Call 1500 (Cat)/1500 228</p></div>
    <p class="copy left">Copyright 2012 Trakindo Utama., All rights reserved.</p>
    <p class="menu-footer right">
      <a href="#">About</a>
      <a href="#">Privacy Policy</a>
      <a href="#">FAQ</a>
      <a href="#">Copyright</a>
      <a href="#">Sitemap</a>
      <a href="#">Contact Us</a>
    </p>
    <div class="clear"></div>
  </div>
</footer>
<!--end of Footer -->
</body></html>